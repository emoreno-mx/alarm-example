package com.example.emmxlenovo41.alarmexample;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.emmxlenovo41.alarmexample.data.AlarmDataBaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Alarm> alarmList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AlarmsAdapter aAdapter;
    AlarmDataBaseAdapter alarmDataBaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if(fab != null){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getApplicationContext(),AlarmActivity.class);
                    startActivity(i);

                }
            });
        }

        alarmDataBaseAdapter = new AlarmDataBaseAdapter(this);
        alarmDataBaseAdapter = alarmDataBaseAdapter.open();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        aAdapter = new AlarmsAdapter(alarmList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new ItemDivider(this));
        recyclerView.setAdapter(aAdapter);
        prepareAlarmData();
    }



    public void prepareAlarmData() {
        List<Alarm> alarms = alarmDataBaseAdapter.getAlarms();
        alarmList.addAll(alarms);

        aAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        alarmDataBaseAdapter.close();
    }
}
