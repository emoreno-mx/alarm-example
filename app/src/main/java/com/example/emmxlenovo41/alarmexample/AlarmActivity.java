package com.example.emmxlenovo41.alarmexample;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.emmxlenovo41.alarmexample.data.AlarmDataBaseAdapter;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AlarmActivity extends AppCompatActivity {
    AlarmDataBaseAdapter alarmDBAdapter;
    EditText title, description;
    TimePicker time;
    int hour , minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        alarmDBAdapter = new AlarmDataBaseAdapter(this);
        alarmDBAdapter = alarmDBAdapter.open();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if(fab != null)
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAlarm(view);
            }
        });

        title = (EditText) findViewById(R.id.title);
        description = (EditText) findViewById(R.id.description);
        time = (TimePicker) findViewById(R.id.timePicker);
        time.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int min) {
                hour = hourOfDay;
                minute = min;
            }
        });
    }

    public void saveAlarm(View view) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);

        Date time = calendar.getTime();
        Toast.makeText(this, "The alarm will set off at " + time, Toast.LENGTH_LONG).show();

        SimpleDateFormat sf = new SimpleDateFormat("HH:mm");
        String date = sf.format(time);
        Bundle args = new Bundle();
        args.putString("HOUR",date);
        args.putString("TITLE",title.getText().toString());
        Intent intent = new Intent(this, WakeUpActivity.class);
        intent.putExtras(args);
        PendingIntent sender = PendingIntent.getActivity(this, 0, intent, 0);
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmMgr.set(AlarmManager.RTC_WAKEUP, time.getTime(), sender);

        alarmDBAdapter.insertEntry(title.getText().toString(), description.getText().toString(), date);

        Intent home = new Intent(this, MainActivity.class);
        startActivity(home);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        alarmDBAdapter.close();
    }
}
