package com.example.emmxlenovo41.alarmexample;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class WakeUpActivity extends Activity {

    private final int NOTIFICATION_ID = 1004;
    TextView hour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Window window = getWindow();
        window.addFlags( WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_wake_up);
        addNotification("wake up");

        hour = (TextView) findViewById(R.id.hour);
        hour.setText(getIntent().getExtras().get("HOUR").toString());
    }

    public void dismiss(View view) {
        NotificationManager notificationMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationMgr.cancel(NOTIFICATION_ID);
        this.finish();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void addNotification(String title) {
        NotificationManager notificationMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new Notification.Builder(this)
                .setContentTitle(title)
                .setSmallIcon(android.R.drawable.ic_lock_idle_alarm)
                .setAutoCancel(false)
                .build();
        notification.defaults|= Notification.DEFAULT_SOUND;
        notification.defaults|= Notification.DEFAULT_LIGHTS;
        notification.defaults|= Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_INSISTENT;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationMgr.notify(NOTIFICATION_ID, notification);
    }
}
