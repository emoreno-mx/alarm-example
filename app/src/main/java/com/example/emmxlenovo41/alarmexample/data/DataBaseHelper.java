package com.example.emmxlenovo41.alarmexample.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper{
    static final String DATABASE_NAME = "alarm.db";
    static final int DATABASE_VERSION = 1;

    public DataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Called when no database exists in disk and the helper class needs to create a new one.
    @Override
    public void onCreate(SQLiteDatabase _db){
        _db.execSQL(AlarmDataBaseAdapter.DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion){
        // Log the version upgrade.
        Log.w("TaskDBAdapter", "Upgrading from version " +_oldVersion + " to " +_newVersion + ", which will destroy all old data");
        // KILL PREVIOUS TABLES IF UPGRADED
        _db.execSQL("DROP TABLE IF EXISTS " + AlarmDataBaseAdapter.TABLE_NAME);
        // CREATE NEW INSTANCE OF TABLES
        onCreate(_db);
    }

}

