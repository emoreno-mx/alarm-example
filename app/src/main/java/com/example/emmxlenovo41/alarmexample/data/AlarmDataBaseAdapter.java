package com.example.emmxlenovo41.alarmexample.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.emmxlenovo41.alarmexample.Alarm;

import java.util.ArrayList;
import java.util.List;

public class AlarmDataBaseAdapter {
    static final String TABLE_NAME = "ALARM";

    // SQL Statement to create a new table.
    static final String DATABASE_CREATE = "CREATE TABLE "+TABLE_NAME+"(IDALARM INTEGER PRIMARY KEY AUTOINCREMENT,TITLE  text,DESCRIPTION text, HOUR text) ";
    // Database instance
    public static SQLiteDatabase db;
    // Context of the app using the database.
    private final Context context;
    // Database helper
    private DataBaseHelper dbHelper;


    public AlarmDataBaseAdapter(Context _context){
        context = _context;
        dbHelper = new DataBaseHelper(context);
    }

    public  AlarmDataBaseAdapter open() throws SQLException{
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        db.close();
    }


    public void insertEntry(String title,String description, String date){
        ContentValues newValues = new ContentValues();
        newValues.put("TITLE", title);
        newValues.put("DESCRIPTION",description);
        newValues.put("HOUR",date);

        // Insert the row into your table
        db.insert(TABLE_NAME, null, newValues);
    }

    public static List<Alarm> getAlarms(){
        List<Alarm> alarms = new ArrayList<>();
        Cursor cursor= db.query(TABLE_NAME, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            String title = cursor.getString(cursor.getColumnIndex("TITLE"));
            String description = cursor.getString(cursor.getColumnIndex("DESCRIPTION"));
            String date = cursor.getString(cursor.getColumnIndex("HOUR"));

            Alarm a = new Alarm(title,description, date);
            alarms.add(a);
        }

        cursor.close();
        return alarms;
    }
}
