package com.example.emmxlenovo41.alarmexample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AlarmsAdapter extends RecyclerView.Adapter<AlarmsAdapter.MyViewHolder> {

    private List<Alarm> alarmList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title , date;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.hour);
        }
    }


    public AlarmsAdapter(List<Alarm> contactsList) {
        this.alarmList = contactsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.alarm_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Alarm alarm = alarmList.get(position);
        holder.title.setText(alarm.getTitle());
        holder.date.setText(alarm.getDate());

    }

    @Override
    public int getItemCount() {
        return alarmList.size();
    }

}